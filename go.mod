module modernc.org/kv

go 1.16

require (
	modernc.org/exp v1.0.0
	modernc.org/fileutil v1.1.2
	modernc.org/internal v1.0.7
	modernc.org/lldb v1.0.6
	modernc.org/mathutil v1.5.0
)

require (
	golang.org/x/sys v0.8.0 // indirect
	modernc.org/bufs v1.0.0 // indirect
)
