# kv

Package kv implements a simple and easy to use persistent key/value (KV) store.

## Build status

available at https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fkv

Installation

    $ go get modernc.org/kv

Documentation: [godoc.org/modernc.org/kv](http://godoc.org/modernc.org/kv)
